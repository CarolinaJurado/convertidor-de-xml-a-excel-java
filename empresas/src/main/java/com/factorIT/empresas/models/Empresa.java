package com.factorIT.empresas.models;

import lombok.Getter;
import lombok.Setter;

public class Empresa {

	@Getter @Setter
	private Long contract_no;

	@Getter @Setter
	private String cuit;
	
	@Getter @Setter
	private String denomination;
	
	@Getter @Setter
	private String adress;
	
	@Getter @Setter
	private Long cp;
	
	@Getter @Setter
	private Long producer;
	
	@Getter @Setter
	private Double balance_ctacte;
	
	@Getter @Setter
	private String concept;
	
	@Getter @Setter
	private Double amount;

	
}
