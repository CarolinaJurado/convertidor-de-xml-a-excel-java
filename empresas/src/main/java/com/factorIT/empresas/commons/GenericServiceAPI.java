package com.factorIT.empresas.commons;

import java.io.ByteArrayInputStream;
import java.util.List;

import org.springframework.stereotype.Component;

import com.factorIT.empresas.models.Empresa;

@Component
public interface GenericServiceAPI{
	
	List<Empresa> getAll();
	
	Boolean validateXMLSchema(String xsdPath, String xmlPath) throws Exception;
	
	ByteArrayInputStream exportAllData() throws Exception;
}