package com.factorIT.empresas.commons;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.factorIT.empresas.models.Empresa;

@Component
public class GenericServiceImpl implements GenericServiceAPI {

	@Override
	public List<Empresa> getAll() {
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        Document document = null;
       
			
				try {
					document = builder.parse(new File("C:/spring suite/workspace/upload/examen-FIT.xml"));
				} catch (SAXException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		
        
        
        List<Empresa> returnList = new ArrayList<>();
        
        
        NodeList nodeList = document.getDocumentElement().getChildNodes();
        
        
        for (int i = 0; i < nodeList.getLength(); i++) {
        	
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
            	
                Element elem = (Element) node;
                
                Empresa empresa = new Empresa();
                
                Long contract_no = Long.parseLong(elem.getElementsByTagName("NroContrato").item(0).getChildNodes().item(0).getNodeValue());
                String cuit = elem.getElementsByTagName("CUIT").item(0).getChildNodes().item(0).getNodeValue();
                String denomination = elem.getElementsByTagName("Denominacion").item(0).getChildNodes().item(0).getNodeValue();
                String adress = elem.getElementsByTagName("Domicilio").item(0).getChildNodes().item(0).getNodeValue();
                Long cp = Long.parseLong(elem.getElementsByTagName("CodigoPostal").item(0).getChildNodes().item(0).getNodeValue());
                Long producer = Long.parseLong(elem.getElementsByTagName("Productor").item(0).getChildNodes().item(0).getNodeValue());
                Double balance_ctacte = Double.parseDouble(elem.getElementsByTagName("SaldoCtaCte").item(0).getChildNodes().item(0).getNodeValue());
                String concept = elem.getElementsByTagName("Concepto").item(0).getChildNodes().item(0).getNodeValue();
                Double amount = Double.parseDouble(elem.getElementsByTagName("Importe").item(0).getChildNodes().item(0).getNodeValue());
                
                empresa.setContract_no(contract_no);
                empresa.setCuit(cuit);
                empresa.setDenomination(denomination);
                empresa.setAdress(adress);
                empresa.setCp(cp);
                empresa.setProducer(producer);
                empresa.setBalance_ctacte(balance_ctacte);
                empresa.setConcept(concept);
                empresa.setAmount(amount);
                
                returnList.add(empresa);
            }
        }
		
		return returnList;
        
	}
        
	@Override
	public Boolean validateXMLSchema(String xsdPath, String xmlPath) throws Exception{
		try {
			SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new File(xsdPath));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new File(xmlPath)));
        } catch (IOException | SAXException e) {
            System.out.println("Exception: "+e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }    

	@Override
	public ByteArrayInputStream exportAllData() throws Exception {
		String[] columns1 = {"Nro de Contrato", "CUIT", "Denominación", "Dirección", "Codigo postal", "Productor" };//columnas de la hoja 1
		String[] columns2 = {"Nro de Contrato","Saldo", "Concepto", "Importe"};//columnas de la hoja 2

		Workbook workbook = new HSSFWorkbook();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();

		Sheet sheet1 = workbook.createSheet("Empresas");//nombre de la hoja 1
		Sheet sheet2 = workbook.createSheet("Movimientos");//nombre de la hoja 2
		Row row1 = sheet1.createRow(0);
		Row row2 = sheet2.createRow(0);

		for (int i = 0; i < columns1.length; i++) {
			Cell cell1 = row1.createCell(i);
			cell1.setCellValue(columns1[i]);
		}
		
		for (int i = 0; i < columns2.length; i++) {
			Cell cell2 = row2.createCell(i);
			cell2.setCellValue(columns2[i]);
		}
		
		DataFormat fmt = workbook.createDataFormat();
		CellStyle nm = workbook.createCellStyle();
		nm.setDataFormat(fmt.getFormat("############"));


		List<Empresa> empresas = this.getAll();
		int initRow = 1;
		for (Empresa empresa : empresas) {
			row1 = sheet1.createRow(initRow);
			row2 = sheet2.createRow(initRow);

			//Ingresando datos a la hoja 1
			row1.createCell(0).setCellValue(empresa.getContract_no());
			row1.getCell(0).setCellStyle(nm);
			row1.createCell(1).setCellValue(empresa.getCuit());
			row1.createCell(2, CellType.STRING).setCellValue(empresa.getDenomination());
			row1.createCell(3, CellType.STRING).setCellValue(empresa.getAdress());
			row1.createCell(4, CellType.NUMERIC).setCellValue(empresa.getCp());
			row1.createCell(5, CellType.NUMERIC).setCellValue(empresa.getProducer());
			
			
			//Ingresando datos a la hoja 2
			row2.createCell(0, CellType.NUMERIC).setCellValue(empresa.getContract_no());
			row2.createCell(1, CellType.NUMERIC).setCellValue(empresa.getBalance_ctacte());
			row2.createCell(2, CellType.STRING).setCellValue(empresa.getConcept());
			row2.createCell(3, CellType.NUMERIC).setCellValue(empresa.getAmount());
			 

			initRow++;
		}

		workbook.write(stream);
		workbook.close();
		return new ByteArrayInputStream(stream.toByteArray());
	}





}
