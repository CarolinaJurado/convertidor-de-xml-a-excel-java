package com.factorIT.empresas.controllers;

import java.io.ByteArrayInputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.factorIT.empresas.commons.GenericServiceAPI;


@Controller
public class empresasController {
	
	@Autowired
	private GenericServiceAPI genericServiceAPI;
	
	@RequestMapping("/")
	public String index(Model model) {
		try {
			//Se agrega un validador por medio de un esquema xsd para que sea mas sencillo de mantener si cambia la cantidad de elementos.
			if(genericServiceAPI.validateXMLSchema("C:/spring suite/workspace/upload/schema.xsd", "C:/spring suite/workspace/upload/examen-FIT.xml")) {
			model.addAttribute("list", genericServiceAPI.getAll());
			return "index";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//no pude lograr mostrar el error por pantalla asi que lo pase por consola.
		return "error";
	}
	
	@GetMapping("/export/all")
	public ResponseEntity<InputStreamResource> exportAllData() throws Exception {
		ByteArrayInputStream stream = genericServiceAPI.exportAllData();

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename=Empresas.xls");

		return ResponseEntity.ok().headers(headers).body(new InputStreamResource(stream));
	}
}
